﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {

    
    public int healthRestore = 20;
    public string healthTag = ""; 

   
	
    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag(healthTag)){
            other.SendMessage("RestoreHealth", healthRestore);
        }

        Destroy(gameObject);
        
    }
}
