﻿using UnityEngine;
using System.Collections;
using UnityEditor.SceneManagement;

public class PlayButton : MonoBehaviour
{

    public string sceneName = "";

    public void onClick()
    {
        EditorSceneManager.LoadScene(sceneName);
    }

}
