﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swingAbility : MonoBehaviour {

    public Rigidbody ropeSwing;
    public Rigidbody playerController;

	
	// Update is called once per frame
	void onCollisionEnter(Collision collision) {
        if(collision.gameObject.name == "Rope Holder")
        {
            Debug.Log("Player collided with rope");
            playerController.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }

    }
}
