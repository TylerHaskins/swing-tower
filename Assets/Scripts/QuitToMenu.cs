﻿using UnityEngine;
using System.Collections;

public class QuitToMenu : MonoBehaviour {

    public delegate void Quit();
    public static event Quit OnQuitClick;
    public string levelName = "";

    public void onClick()
    {
        Application.LoadLevel(levelName);
    }
}
