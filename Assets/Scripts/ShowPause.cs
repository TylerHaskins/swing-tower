﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting;

public class ShowPause : MonoBehaviour {

    public GameObject pauseMenu;
    private bool isVisible;


    void Start()
    {
        pauseMenu.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            isVisible = !isVisible;
            pauseMenu.SetActive(isVisible);
            Time.timeScale = 0.0000000001f;
        }
        else if (isVisible == false)
        {
            Time.timeScale = 1.0f;
        }
    }
}