﻿using UnityEngine;
using System.Collections;

public class MenuButtonPlay : MonoBehaviour {

    public string levelName = "";

    public void onClick()
    {
        Time.timeScale = 1.0f;
        Application.LoadLevel(levelName);
    }
	
	
}
